Using network 'development'.


Compiling your contracts...
===========================
> Compiling ./contracts/decentralized-resolver/Resolver.sol
> Artifacts written to /tmp/test-2020319-1559821-175uc03.n5py
> Compiled successfully using:
   - solc: 0.5.16+commit.9c3226ce.Emscripten.clang

resolver 0xB83B09ECC032b43138eA8FD7ED65972E10140F52

[0m[0m
[0m  Contract: resolver[0m
  [32m  ✓[0m[90m constuct[0m
  [32m  ✓[0m[90m register[0m[31m (89ms)[0m
  [31m  1) update[0m

    Events emitted during test:
    ---------------------------

    Resolver.NewUpdate(
      addr: [33m0x2611E42943bf67B7A90CE5ba893100142bC79083[39m (type: address),
      update: [33m0x2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae[39m (type: bytes32)
    )


    ---------------------------


[92m [0m[32m 2 passing[0m[90m (419ms)[0m
[31m  1 failing[0m

[0m  1) Contract: resolver
       update:

      [31mAssertionError [ERR_ASSERTION]: '0x0000000000000000000000000000000000000000000000000000000000000000' == '0x2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae'[0m
      [32m+ expected[0m [31m- actual[0m

      [31m-0x0000000000000000000000000000000000000000000000000000000000000000[0m
      [32m+0x2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae[0m
      [0m[90m
      at Context.<anonymous> (/home/lash/src/home/decentralized-resolver/project/test/Resolver.js:25:10)
      at processTicksAndRejections (internal/process/task_queues.js:97:5)
[0m


