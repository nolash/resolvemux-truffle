let Resolver = artifacts.require('Resolver');

module.exports = async function(deployer, network, accounts) {
	deployer.then(async () => {
		let resolver = await deployer.deploy(Resolver, 'foo');
		console.log('resolver', resolver.address);
	});
};
