let assert = require('assert');

let Resolver = artifacts.require('Resolver');

let fooSha256 = '0x2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae';

contract('resolver', (accounts) => {
	it('constuct', async () => {
		let resolver = await Resolver.deployed();
		assert.equal(await resolver.name(), 'foo');
		assert.ok(await resolver.init(), true);
	});

	it('register', async () => {
		let resolver = await Resolver.deployed();
		assert.ok(!await resolver.isRegistered(accounts[0]));
		await resolver.register();
		assert.ok(await resolver.isRegistered(accounts[0]));
		await resolver.register({from: accounts[1]});
		assert.ok(await resolver.isRegistered.call(accounts[1]));
	});

	it('update', async () => {
		let resolver = await Resolver.deployed();
		assert.equal(await resolver.getUpdate(accounts[0]), 0);
		let r = await resolver.update(fooSha256);
		assert.equal(await resolver.getUpdate(accounts[0]), fooSha256);
	});

	it('location', async () => {
		let resolver = await Resolver.deployed();
		let r = await resolver.addLocation('foo');
		//const ev = await resolver.getPastEvents();
		//
		let u = await resolver.updateLocation(0, fooSha256);
	});
});
