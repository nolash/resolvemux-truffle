let Web3 = require('web3');
let bzz = require('swarm-lowlevel');
let ipfshash = require('ipfs-only-hash');
let swarm = require('swarm-lowlevel');
let Resolver = artifacts.require('Resolver');

contract('resolver', (accounts) => {

	w3 = new Web3();

	it('example', async () => {
		let resolver = await Resolver.deployed();
		await resolver.register({from: accounts[1]});

		let indata = 'bar';
		let indata_buffer  = Buffer.from(indata);

		// add the update stem
		let h = w3.utils.sha3(indata);
		await resolver.update(h, {from: accounts[1]});

		let locations_idx = {
			'foo':	0,
			'bar':	1,
			'ipfs':	2,
			'bzz':	3,
		};

		let locations = [
			'http://foo.com',
			'http://bar.org/xyzzy',
			'ipfs://',
			'bzz://',
		];

		for (let i = 0; i < 4; i++) {
			let r = await resolver.addLocation(locations[i], {from: accounts[1]});
		};

		// add non sha3 content addressed locations
		// ipfs..
		let ipfs_location = await ipfshash.of(indata_buffer);
		let ipfs_location_hex = w3.utils.asciiToHex(ipfs_location);
		await resolver.updateLocation(
			locations_idx['ipfs'],
			ipfs_location_hex,
			{
				from: accounts[1],
			},
		);

		// swarm..
		let bmtHasher = new bzz.bmtHasher();
		bmtHasher.ResetWithLength(indata_buffer.length);
		bmtHasher.Write(indata_buffer);
		let bzz_location = bmtHasher.Sum();
		let bzz_location_hex = w3.utils.bytesToHex(bzz_location);
		await resolver.updateLocation(
			locations_idx['bzz'],
			bzz_location_hex,
			{
				from: accounts[1],
			},
		);

		// get locations
		let u = await resolver.getUpdate.call(accounts[1]);
		let subl = u.substring(2);

		let l = await resolver.getLocation(accounts[1], locations_idx['foo']);
		console.log('location: foo', l[0] + '/' + u);

		l = await resolver.getLocation(accounts[1], locations_idx['bar']);
		console.log('location: bar', l[0] + '/' + u);

		l = await resolver.getLocation(accounts[1], locations_idx['ipfs']);
		subl = web3.utils.hexToAscii(l[1]);
		console.log('location: ipfs', l[0] + subl);

		l = await resolver.getLocation(accounts[1], locations_idx['bzz']);
		subl = l[1];
		console.log('location: bzz', l[0] + subl);


	});
});
